package com.epam.rd.java.basic.task8.elements;

public class VisualParameters {
    public String stemColour;
    public String leafColour;
    public String measure;
    public int aveLenFlower;
}
