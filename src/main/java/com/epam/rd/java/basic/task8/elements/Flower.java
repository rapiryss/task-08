package com.epam.rd.java.basic.task8.elements;

public class Flower {
    public String name;
    public String soil;
    public String origin;
    public VisualParameters vp;
    public GrowingTips gt;
    public String multiplying;

    public String getName() {
        return name;
    }

    public String toString() {
        String b=(gt.lighting)?"\"yes\"":"\"no\"";
        return "<flower>\n" +
                "        <name>"+name+"</name>\n" +
                "        <soil>"+soil+"</soil>\n" +
                "        <origin>"+origin+"</origin>\n" +
                "        <visualParameters>\n" +
                "            <stemColour>"+vp.stemColour+"</stemColour>\n" +
                "            <leafColour>"+vp.leafColour+"</leafColour>\n" +
                "            <aveLenFlower measure=\""+vp.measure+"\">"+vp.aveLenFlower+"</aveLenFlower>\n" +
                "        </visualParameters>\n" +
                "        <growingTips>\n" +
                "            <tempreture measure=\""+gt.measureT+"\">"+gt.tempreture+"</tempreture>\n" +
                "            <lighting lightRequiring="+b+"/>\n" +
                "            <watering measure=\""+gt.measureW+"\">"+gt.watering+"</watering>\n" +
                "        </growingTips>\n" +
                "        <multiplying>"+multiplying+"</multiplying>\n" +
                "    </flower>\n";
    }
}

